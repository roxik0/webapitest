﻿using System;

namespace BajtkowoPhone
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Console MessageApp Emulator");
                Console.WriteLine("1. Set your phone number");
                Console.WriteLine("2. SendMessage");
                Console.WriteLine("3. CheckMessage");
                Console.WriteLine("4. Exit");

                var key=Console.ReadKey();
                if (key.Key == ConsoleKey.D1)
                {
                    //Here register your number in webApi
                }
                if (key.Key == ConsoleKey.D2)
                {
                    //Here send a message
                }
                if (key.Key == ConsoleKey.D3)
                {
                    //Here read a message to you
                }
                if (key.Key == ConsoleKey.D4)
                {
                    return;
                }
            }


        }
    }
}
